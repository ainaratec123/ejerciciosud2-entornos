package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner escaner = new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena1 = escaner.nextLine();
		System.out.println("Introduce una segunda cadena");
		String cadena2 = escaner.nextLine();
		
		
		Metodos.iguales(cadena1, cadena2);
		
		Metodos.contador(cadena1, cadena2);
		
		Metodos.caracter(cadena1, cadena2);
		escaner.close();
		
	}

}
